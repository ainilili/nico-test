import java.util.HashMap;
import java.util.Map;

class Test {
	
	public int lengthOfLongestSubstring(String s) {
        Integer[] table = new Integer[1024];
        char[] cs = s.toCharArray();
        if(cs.length == 0) {
        	return 0;
        }
        int max = 1;
        int start = 0;
        int end = 0;
        for(int index = 0; index < cs.length; index ++) {
        	char c = cs[index];
        	Integer temp = table[c];
        	if(temp != null && temp >= start) {
        		int len = end - start;
        		start = temp + 1;
        		table[c] = index;
        		max = max < len ? len : max;
        	}else {
        		table[c] = index;
        	}
        	end ++;
        }
        int re = end - start;
        return max > re ? max : re;
    }
    
	public static void main(String[] args) {
		System.out.println(new Test().lengthOfLongestSubstring("au"));
	}
}
