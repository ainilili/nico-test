import java.util.Arrays;

public class Test1 {

	public static void main(String[] args) {
		
//		String str1 = "java.util.Map<java.lang.String, ? extends com.jiujun.voice.apps.user.domain.UserInfo>[]";
//		String str2 = "java.util.Map<java.lang.Integer, com.jiujun.voice.apps.user.domain.UserInfo>";
		String str3 = "java.util.Map<java.util.Map<java.lang.Integer, java.util.Map<java.lang.String, ? extends com.jiujun.voice.apps.user.domain.UserInfo>[]>[], com.jiujun.voice.apps.user.domain.UserInfo>";
	
//		System.out.println(fn(str1));
//		System.out.println(fn(str2));
		System.out.println(fn(str3));
	}
	
	public static String fn(String str) {
		StringBuilder builder = new StringBuilder();
		
		String[] ss = str.split("<");
		for(String s: ss) {
			if(s.contains(",")) {
				String[] sss = s.split(",");
				for(String st: sss) {
					String[] ssss = st.split("[.]");
					builder.append(ssss[ssss.length - 1] + ",");
				}
				builder.deleteCharAt(builder.length() - 1).append("<");
			}else {
				String[] sss = s.split("[.]");
				builder.append(sss[sss.length - 1] + "<");
			}
		}
		return builder.deleteCharAt(builder.length() - 1).toString();
	}
}
