package org.nico.ourbatis.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

public class Test {
	
	public static void main(String[] args) throws FileNotFoundException {
		
		File f = new File("C:\\Users\\admin\\Documents\\Tencent Files\\473048656\\FileRecv\\Script.jmx");
		FileInputStream fis = new FileInputStream(f);
		
		SmartScanner ss = new SimpleScanner(StreamUtils.convertToString(fis));
		List<Document> docs = ss.scan().results();
		
		DocumentUtils.showDocuments(docs);
		
		NoelFormat nf = new NoelFormat(docs);
		System.out.println(nf.format().result());
	}
}
