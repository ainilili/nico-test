import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.nico.ourbatis.xml.Document;
import org.nico.ourbatis.xml.DocumentType;
import org.nico.ourbatis.xml.DocumentUtils;
import org.nico.ourbatis.xml.NoelFormat;
import org.nico.ourbatis.xml.SimpleScanner;
import org.nico.ourbatis.xml.SmartScanner;
import org.nico.ourbatis.xml.StreamUtils;

public class Test4 {

	public static void main(String[] args) throws FileNotFoundException {
		
		//解析xml
		File f = new File(System.getProperty("user.dir") + "/src/Script.jmx");
		FileInputStream fis = new FileInputStream(f);
		SmartScanner ss = new SimpleScanner(StreamUtils.convertToString(fis));
		List<Document> docs = ss.scan().results();
//		DocumentUtils.showDocuments(docs);
		NoelFormat nf = new NoelFormat(docs);
		System.out.println(nf.format().result());
		
		//组装xml
		docs = new ArrayList<>();
		
		Document header = new Document();
		header.setName("?xml");
		header.setParameters(new LinkedHashMap<String, String>() {{
			put("version", "1.0");
			put("encoding", "UTF-8");
		}});
		header.setParameterString(DocumentUtils.formatParameters(header.getParameters()));
		header.setType(DocumentType.SINGLE);
		header.setTail("?");
		
		Document root = new Document();
		root.setName("jmeterTestPlan");
		root.setParameters(new LinkedHashMap<String, String>() {{
			put("version", "1.2");
			put("properties", "5.0");
			put("jmeter", "5.0 r1840935");
		}});
		root.setParameterString(DocumentUtils.formatParameters(root.getParameters()));
		root.setType(DocumentType.DOUBLE);
		root.setTail("");
		
		Document child = new Document();
		child.setName("hashTree");
		child.setType(DocumentType.DOUBLE);
		child.setContent("hello ourbatis xml");
		child.setTail("");
		
		root.setChilds(new ArrayList<Document>() {{
			add(child);
		}});
		
		docs.add(header);
		docs.add(root);
		nf = new NoelFormat(docs);
		System.out.println(nf.format().result());
	}
	
	
}
