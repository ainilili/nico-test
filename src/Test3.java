import java.util.Random;

public class Test3 {

	public static void main(String[] args) {
		
		String s = null;
		for(int i = 0; i < 10; i++) {
			s = "" + i;
		}
		System.out.println(s);
	}
	
	public static void do1() {
		String s = "";
		s = s + 123;
		System.out.println(s);
	}
	
	public static void do2() {
		String s = String.valueOf(123);
		System.out.println(s);
	}
	
	public static void do3() {
		int count = 1000 * 10000;
		long start = System.currentTimeMillis();
		while(count -- > 0) {
			String s = "" + 3;
//			String s = String.valueOf(3);
		}
		long end = System.currentTimeMillis();
		System.out.println(end - start);
	}
	
}
