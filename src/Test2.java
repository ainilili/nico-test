import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Test2 {

	public static void main(String[] args) {
//		test1();
		test2();
	}

	public static void test1() {
		int count = 10000;
		int sum = 0;
		long start = System.currentTimeMillis();
		while(count -- > 0) {
			sum += 1;
		}
		long end = System.currentTimeMillis();
		System.out.println(sum + "-" + (end - start) + "ms");
	}
	
	public static void test2() {
		int count = 10000000;
		Integer sum = 0;
		Random random = new Random();
		long start = System.currentTimeMillis();
		
		List<Integer> list = new ArrayList<>(10000);
		while(count -- > 0) {
			
			list.add(random.nextInt(10000));
		}
		
		for(Integer n: list) {
			sum += n;
		}
		long end = System.currentTimeMillis();
		System.out.println(sum + "-" + (end - start) + "ms");
	}
}
